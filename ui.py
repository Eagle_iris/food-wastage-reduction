# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 17:36:58 2019

@author: Anunivas
"""

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tkinter as tk 
import statsmodels.api as sm

# Importing the dataset
dataset = pd.read_csv('data.csv')
X1 = dataset.iloc[:, 0:3].values
y1 = dataset.iloc[:, 3].values
X2 = X1
y2 = y1

# tkinter GUI
root= tk.Tk() 
 
canvas1 = tk.Canvas(root, width = 600, height = 450)
canvas1.pack()

label = tk.Label(root, text='Student Expected count analysis for the provided Date, Day, Menu')
canvas1.create_window(270, 55, window=label)

# New_Interest_Rate label and input box
label1 = tk.Label(root, text='Date : ')
canvas1.create_window(170, 120, window=label1)

entry1 = tk.Entry (root) # create 1st entry box
canvas1.create_window(300, 120, window=entry1)

# New_Unemployment_Rate label and input box
label2 = tk.Label(root, text='Day  : ')
canvas1.create_window(170, 150, window=label2)

entry2 = tk.Entry (root) # create 2nd entry box
canvas1.create_window(300, 150, window=entry2)

# New_Unemployment_Rate label and input box
label3 = tk.Label(root, text='Menu : ')
canvas1.create_window(170, 180, window=label3)

entry3 = tk.Entry (root) # create 2nd entry box
canvas1.create_window(300, 180, window=entry3)


def values(): 
    global New_Date #our 1st input variable
    New_Date = entry1.get()
    
    global New_Day #our 2nd input variable
    New_Day = entry2.get()
    
    global New_Menu #our 2nd input variable
    New_Menu = entry3.get()
    A=[New_Date,New_Day, New_Menu]
    X1=np.vstack([X2,A])
    B=[0]
    y1=np.concatenate((y2,B))
    from sklearn.preprocessing import LabelEncoder
    labelencoder_X1 = LabelEncoder()
    X1[:, 0] = labelencoder_X1.fit_transform(X1[:, 0])
    X1[:, 1] = labelencoder_X1.fit_transform(X1[:, 1])
    X1[:, 2] = labelencoder_X1.fit_transform(X1[:, 2])
    from sklearn.linear_model import LinearRegression
    regr = LinearRegression()
    regr.fit(X1,y1)
    print('Intercept: \n', regr.intercept_)
    print('Coefficients: \n', regr.coef_)
    Prediction_result  = ('Students expected for the day : ', regr.predict(X1[-1: , :]))
    label_Prediction = tk.Label(root, text= Prediction_result, bg='skyblue')
    canvas1.create_window(260, 300, window=label_Prediction)
    
button1 = tk.Button (root, text='Predict count',command=values, bg='skyblue') # button to call the 'values' command above 
canvas1.create_window(270, 245, window=button1)
 

root.mainloop()